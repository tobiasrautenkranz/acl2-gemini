(in-package "GEMINI")

(include-book "gemini")

(defxdoc gemini
  :short "Gemini protocol ABNF grammar.")
