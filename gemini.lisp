;; ABNF Grammer of the gemini protocol for ACL2
;;
;; https://geminiprotocol.net/docs/protocol-specification.gmi
;; https://www.kestrel.edu/people/coglio/vstte18.pdf
;;

(in-package "GEMINI")
;(in-package "ABNF")

(include-book "kestrel/abnf/grammar-parser/top" :dir :system)
(include-book "kestrel/abnf/grammar-definer/top" :dir :system)
(include-book "kestrel/abnf/top" :dir :system)

(abnf::defgrammar *mime-grammar-rules*
  :short "RFC 2045"
  :file "mime.abnf"
  :untranslate t
  :well-formed t)

(defthm mime-wellformed
  (abnf::rulelist-wfp *mime-grammar-rules*))

(defthm mime-not-closed
    (not (abnf::rulelist-closedp *mime-grammar-rules*)))

(abnf::defgrammar *gemini-grammar-rules*
  :short "The gemini grammar rules."
  :file "gemini.abnf"
  :untranslate t
  :well-formed t)

#|
(defun plug-rules (target sources)
  (declare (xargs
            :guard (true-listp sources)
            :measure (length sources)))
  (if sources
      (plug-rules
       (abnf::plug-rules target (first sources))
       (rest sources))
      target))
|#

(defval *all-gemini-grammar-rules*
  :short "All the grammar rules,
          including the referenced URI rules and ABNF core rules."
  :long "TODO"
; (plug-rules *gemini-grammar-rules*
;              (list
;              abnf::*uri-grammar-rules*
;;              *mime-grammar-rules*
;              abnf::*core-rules*)))
 (abnf::plug-rules
   (abnf::plug-rules
    (abnf::plug-rules *gemini-grammar-rules*
                     abnf::*uri-grammar-rules*)
    *mime-grammar-rules*)
   abnf::*core-rules*)

   ///

  (defthm gemini-wellformed
      (abnf::rulelist-wfp *all-gemini-grammar-rules*))

;; FIXME define these types to make the def closed
;; can not rely on rfc2045 due to future dependencies.
;; see: thm mime-not-closed
;type = "6"
;subtype = "6"
;parameter = "6"
  (defthm gemini-not-closed
      (not (abnf::rulelist-closedp *all-gemini-grammar-rules*))))


;  (abnf::pretty-print-rulelist *all-gemini-grammar-rules*)
