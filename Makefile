all: gemini.cert

# path to the acl cert.pl
ACL_CERT_PL ?= $(shell echo "(quit)" | acl2 | awk '/:SYSTEM/ { print gensub("\"([^\"]*)\".*", "\\1build/cert.pl", "g", $$3) }')

.PHONY: run
run: gemini.cert
	echo '(include-book "gemini") (abnf::pretty-print-rulelist gemini::*all-gemini-grammar-rules*) (quit)' | acl2

gemini.cert: *.lisp *.abnf *.lsp *.acl2
	${ACL_CERT_PL} *.lisp


.PHONY: clean
clean:
	rm -f -- *.fasl *.cert *.cert.out top.port Makefile-tmp *.port
